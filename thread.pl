#!/usr/bin/env perl

print "# Test Threads In Perl \n";

use threads;
use threads::shared;

sub thrd($) {
  my $num = shift;
  print "   In-thread :: [$num] \n";
  #threads->exit(0);
  #return 0;
}


for ($n = 0; $n < 6000; $n++) {
  print "=== Create thread [$n] ===\n";
  threads->create(\&thrd, ($n));
};

sleep 20;
$_->join for(threads->list());
print "=== End. === \n";
